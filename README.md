# Backtest_rsi

#### 介绍
CSDN 文章 "在 Elasticsearch 中回测 RSI 交叉策略”的演示材料
(https://blog.csdn.net/dbsrhouse/article/details/118666773)

#### 软件架构
软件架构说明
Curl shell 脚本和 Python 访问 Elasticsearch 服务器（使用 v7.10.1 测试)

使用说明
执行此脚本将创建nav索引，并将文档数据索引其中。索引pts的数据来自TuShare大数据开放社区，其中选择了54只工银瑞信股票型基金用于演示目的。选择的时间范围在2020-12-01和2021-05-31之间。

./nav_index.sh

Python 安装教程
自动运行 backtest_rsi.sh 中的 pip install并激活虚拟环境命令

执行此脚本将索引pts下计算基金代码000803.OF在2021-01-01和2021-05-31期间回测 RSI 交叉策略。

./backtest_rsi.sh 000803.OF

执行结果生成RSI交叉交易策略统计数据
购买次数:             1
卖出次数:             1
得胜次数:             1
亏损次数:             0
总利润  :        0.2320
总利润/购买次数:   0.2320
最高购买价格:        2.9320
利润百分率:        7.91%


参与贡献
Fork 本仓库
新建 Feat_xxx 分支
提交代码
新建 Pull Request
