import requests
import json
import sys, getopt
import pprint

# get data from elasticsearch server 
def get_data(inputfile, symbol):
    url = 'http://localhost:9200/nav/_search?pretty'
    with open(inputfile) as f:
        payload = json.load(f)
    payload_json = json.dumps(payload)
    payload_json = payload_json % symbol
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    r = requests.post(url, data=payload_json, headers=headers)
    return r.text

# get the command line parameters for the trading policy and the ticker symbol
def get_opt(argv):
    inputfile = ''
    symbol = 'FDEV'
    try:
        opts, args = getopt.getopt(argv, "hi:s:")
    except getopt.GetoptError:
        print('backtest_rsi -i <inputfile> -s <symbol>')
        print('example: backtest_rsi -i backtest_rsi.json -s FDEV')

    for opt, arg in opts:
        if opt == '-h':
            print('backtest_rsi -i <inputfile>')
            print('example: backtest_rsi -i backtest_rsi.json -s FDEV')
            sys.exit(0)
        elif opt in ('-i'):
            inputfile = arg
        elif opt in ('-s'):
            symbol = arg

    if inputfile == '':
        print("No input file!")
        sys.exit(-1)

    return inputfile, symbol


# parse the response data and refine the buy/sell signal
def parse_data(resp):
    result = json.loads(resp)
    aggregations = result['aggregations']
    if aggregations and 'Backtest_RSI' in aggregations:
        Backtest_RSI = aggregations['Backtest_RSI']

    transactions = []
    hold = False
    if Backtest_RSI and 'buckets' in Backtest_RSI:
        for bucket in Backtest_RSI['buckets']:
            transaction = {}
            transaction['date'] = bucket['key_as_string']
            transaction['Daily'] = bucket['Daily']['value']
            transaction['Daily_unit_nav'] = bucket['Daily_unit_nav']['value']
            # honor buy signal if there is no share hold
            if bucket['RSI_Type']['value'] == 1 and not hold:
                transaction['buy_or_sell'] = 'buy'
                hold = True
            # honor sell signal if there is a share hold
            elif bucket['RSI_Type']['value'] == 2 and hold:
                transaction['buy_or_sell'] = 'sell'
                hold = False
            # for other situations, just hold the action
            else:
                transaction['buy_or_sell'] = 'hold'
            transactions.append(transaction)

    return transactions


def report(transactions):
    print('Transaction Sequence')
    pp = pprint.PrettyPrinter(indent=4)
    print('-' * 80)
    pp.pprint(transactions)
    print('-' * 80)
    print()

    profit = 0.0;
    num_of_buy = 0
    num_of_sell = 0
    buy_adj_nav = 0;
    win = 0
    lose = 0
    max_buy_price = 0
    buy_flag = False
    sell_flag = False
    for transaction in transactions:
        if buy_flag:
           num_of_buy += 1
           buy_adj_nav = transaction['Daily']
           profit -= transaction['Daily']
           max_buy_price = transaction['Daily_unit_nav'] if max_buy_price < transaction['Daily_unit_nav'] else max_buy_price
           buy_flag = False
           continue
        elif sell_flag:
           profit += transaction['Daily']
           if transaction['Daily'] > buy_adj_nav:
               win += 1
           else:
               lose += 1
           buy_adj_nav = 0
           num_of_sell += 1
           sell_flag = False
           continue

        if transaction['buy_or_sell'] == 'buy': 
            buy_flag = True
        elif transaction['buy_or_sell'] == 'sell' and buy_adj_nav>0:
            sell_flag = True

    if buy_adj_nav > 0:
        profit += transactions[-1]['Daily']
        if transaction['Daily'] > buy_adj_nav:
            win += 1
        else:
            lose += 1

    print('购买次数:      %8d' % (num_of_buy))
    print('卖出次数:      %8d' % (num_of_sell))
    print('得胜次数:      %8d' % (win))
    print('亏损次数:      %8d' % (lose))
    print('总利润  :      %8.4f' % (profit))
    if num_of_buy > 0:
        print('总利润/购买次数: %8.4f' % (profit/num_of_buy))
    print('最高购买价格:      %8.4f' % max_buy_price)
    if max_buy_price > 0:
       print('利润百分率:    %8.2f%%' % (profit*100/max_buy_price))


def main(argv):
    inputfile, symbol = get_opt(argv) 
    resp = get_data(inputfile, symbol)
    transactions = parse_data(resp)
    report(transactions)


if __name__ == '__main__':
    main(sys.argv[1:])
